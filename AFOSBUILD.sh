bash compile-cmake.command

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Cmake compile... PASS!"
else
  # houston we have a problem
  exit 1
fi

strip cmake-build/iblessing-linux-all

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Strip... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf cmake-build/iblessing-linux-all /opt/ANDRAX/bin/iblessing

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
